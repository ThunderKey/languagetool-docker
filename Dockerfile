FROM openjdk:8-jre-alpine

RUN apk --no-cache add unzip

ENV VERSION stable

ADD https://www.languagetool.org/download/LanguageTool-$VERSION.zip /tmp/LanguageTool-$VERSION.zip

RUN unzip /tmp/LanguageTool-$VERSION.zip -d /opt/ \
    && rm /tmp/LanguageTool-$VERSION.zip \
    && ln -s /opt/LanguageTool-$VERSION /opt/languagetool

WORKDIR /opt/languagetool

COPY languagetool.sh /opt/languagetool
RUN chmod u+x /opt/languagetool/languagetool.sh

ENV PATH="/opt/languagetool:${PATH}"
