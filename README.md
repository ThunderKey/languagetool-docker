# Usage

```bash
docker run -ti -v $(pwd):/var/docs --rm thunderkey/languagetool languagetool.sh -l en /var/docs/main.txt
```
